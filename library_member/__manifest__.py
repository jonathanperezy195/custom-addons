# -*- coding: utf-8 -*-
{
    'name': "Library Members",
    'description': "Manage people who will be able to borrow books.",
    'author': "My Company",
    # any module necessary for this one to work correctly
    'depends': ['library_app','mail'],
    'application':False,
    # always loaded
    'data': [
        'views/book_view.xml',
        'security/library_security.xml',
        'security/ir.model.access.csv',
        'views/member_view.xml',
        'views/library_menu.xml',
        'views/book_list_template.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'data/library.book.csv',
        'data/book_demo.xml'
    ]
}
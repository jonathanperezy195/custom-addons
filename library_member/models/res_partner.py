from odoo import models, fields

class Partner(models.Model):
    _inherit = "res.partner"

    # relacion inversa de la clase Book hacia Parnert
    book_ids = fields.Many2many(
        comodel_name = 'library.book',
        string = "Authored Books"
    )
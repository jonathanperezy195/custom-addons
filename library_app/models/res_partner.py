from odoo import models, fields

class Partner(models.Model):
    _inherit = 'res.partner'
    
    published_book_ids = fields.One2many(
        'library.book', # modelo relacionado
        'publisher_id', # campo para "this" en el modelo relacionado,
        string = 'Published books'
    )
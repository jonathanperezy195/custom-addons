# -*- coding: utf-8 -*-

from odoo import api, models, fields
from odoo.exceptions import Warning, ValidationError

class Book(models.Model):
    _name = "library.book"
    _description = "Book"
    _order = "name, date_published desc"

    # .Char(string,attrs)
    name = fields.Char('Title', required=True)
    isbn = fields.Char('ISBN')
    # .Boolean(string, attrs)
    active = fields.Boolean('Active?', default=True)
    # .Date()
    date_published =  fields.Date()
    # .Binary(string)
    image = fields.Binary('Cover')
    # .Many2one(modelo_ayudante, strings, attrs)
    publisher_id = fields.Many2one('res.partner',string="Publisher")
    # .Many2many()
    author_ids = fields.Many2many(comodel_name='res.partner',  # modelo relacionado
                                  #relation='library_book_res_partner_rel', # nombre de la tabla a usar
                                  #column1='a_id', # tabla de relacion para "this" record
                                  #column2='p_id', # tabla de relacion para "other" record
                                  string="Authors")
    # .Selection(a,b) , donde a = ([(field,string_value, ...)]) lista cuya posiciones son tuplas de dos posiciones donde [0] es un campo o clave y [1]
    # es el valor o String a mostrar. y b es el valor del campo.
    book_type = fields.Selection([('paper','Paperback'),
                                  ('hard','Hardcover'),
                                  ('electronic','Electronic'),
                                  ('other','Other')], 
                                  'Type')
    # .Text(string)                                    
    notes = fields.Text('Internal Notes')
    # .Html(string)
    descr = fields.Html('Description')
    # .Integer(default)
    copies = fields.Integer(default=1)
    # .Float(string, (maximo_caracteres, decimales))
    avg_rating = fields.Float('Average Rating', (3,2))
    # .Monetary(string, id_uso_moneda)
    price = fields.Monetary('Price','currency_id')

    currency_id = fields.Many2one('res.currency') # ayudante de precios

    publisher_country_id = fields.Many2one(
        'res.country', 
        string="Publisher Country", 
        compute="_compute_publisher_country", # el _compute_publisher_country puede ser una referencia invocable (debe declararse antes)
        inverse='_inverse_publisher_country',
        search='_search_publisher_country',
        store=True # Guardar el resultado del compute
    )

    publisher_country_related = fields.Many2one(
        'res.country',
        string = 'Publisher Country (related)',
        related='publisher_id.country_id',
        readonly=False, # crear campo editable
        store=True
    )
    
    # restricciones sql, sintaxis [(name,code,error),(...)]
    _sql_constraints = [
        ('library_book_name_date_uq', # identificador unico de la restriccion
         'UNIQUE (name, date_published)', # Sinstaxis de la restriccion
         'Book title and publication date must be unique'),

         ('library_book_check_date',  # id unq restriccion
          'CHECK (date_published <= current_date)', # sql sintaxis
          'Publication date must be not be in the future.'
         )
    ]

    @api.multi
    def _check_isbn(self):
        self.ensure_one()
        digits = [int(x) for x in self.isbn if x.isdigit()]
        if len(digits) == 13:
            ponderations = [1,3] * 6
            terms = [a * b for a, b in zip(digits[:12], ponderations)]
            remain = sum(terms) % 10
            check = 10 - remain if remain != 0 else 0
            return digits[-1] == check
    
    @api.multi
    def button_check_isbn(self):
        for book in self:
            if not book.isbn:
                raise Warning('Please provide an ISBN for %s' % book.name)
            if book.isbn and not book._check_isbn():
                raise Warning('%s is an invalid ISBN' % book.isbn)
        return True
    
    def _compute_publisher_country(self):
        for book in self:
            book.publisher_country_id = book.publisher_id.country_id

    def _inverse_publisher_country(self):
        for book in self:
            book.publisher_id.country_id = book.publisher_country_id
    
    def _search_publisher_country(self, operator, value):
        return [('publisher_id.country_id', operator, value)]
    
    @api.constrains('isbn')
    def _constrain_isbn_valid(self):
        for book in self:
            if book.isbn and not book._check_isbn():
                raise ValidationError(
                    '%s is an invalid ISBN' % book.isbn
                )

{
    "name": "Library Checkout",
    "description": "Library Checkout Desc",
    "author":"My company",
    "version": "12.0.1.0",
    "depends": ['library_member'],
    "application":False,
    "data": [
        'security/ir.model.access.csv',
        'views/library_menu.xml',
        'views/checkout_view.xml',
        'data/library_checkout_stage.xml'
    ],
    "demo":[

    ]
}